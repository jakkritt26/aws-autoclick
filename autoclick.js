const puppeteer = require('puppeteer');
import { createWorker } from 'tesseract.js';
 
(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://www.adworld.company/');
  await page.waitForSelector('#ctl00_ContentPlaceHolder1_Captcha_IMG');
  const element = await page.$('#ctl00_ContentPlaceHolder1_Captcha_IMG');  
 
  await element.screenshot({path: 'captcha.png'});
 
  await browser.close();

  const worker = createWorker({
    logger: m => console.log(m)
  });
  
  (async () => {
    await worker.load();
    await worker.loadLanguage('eng');
    await worker.initialize('eng');
    const { data: { text } } = await worker.recognize('https://tesseract.projectnaptha.com/img/eng_bw.png');
    console.log(text);
    await worker.terminate();
  })();

})();

//index.js file
